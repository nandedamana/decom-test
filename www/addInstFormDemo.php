<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/class.php');
	require_once($DELIBDIR.'/php/nan/form.php');
	
	$cobj = new DecomClass('institute');
	$attribs = $cobj->getAttributes();
	
	$inputFields = [];
	$inputFieldLabels = [];
	$inputFieldTypes = [];
	$inputFieldsReq = [];
	$action = '#';
	foreach($attribs as $a) {
		$inputFields[]      = $a->getId();
		$inputFieldLabels[] = $a->getName();
		$inputFieldTypes[]  = ($a->getType() == 'int')? 'number': 'text';
		$inputFieldsReq[]   = true; // TODO FIXME not always needed; Use API fun ($a->getRequired()) when available
	}
	
	echo nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action);
?>
