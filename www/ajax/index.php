<?php
/* Nandakumar Edamana
 * 2019-07-04
 */
	require_once('../delocalconf.php');
	require_once($DELIBDIR.'/php/entity.php');
	require_once($DELIBDIR.'/php/views/entity.php');
	require_once($DELIBDIR.'/php/views/page.php');

	decom_page_init();
	decom_page_set_title('My Dynamic Page');
	$content = '<div id="kudam"></div><button id="btnFill">നിറയ്ക്കുക</button>';
	decom_page_set_content($content);
	decom_page_display();
?>
<script>
document.addEventListener('DOMContentLoaded', function() {
	btnFill = document.getElementById('btnFill');
	kudam = document.getElementById('kudam');
	
	btnFill.addEventListener('click', function() {
		xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200)
				kudam.innerHTML = this.response;
		};
		xhr.open('GET', 'getpinfo.php?eid=2', true);
		xhr.send();
	});
});
</script>
