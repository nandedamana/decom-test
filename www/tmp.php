<?php

require_once('delocalconf.php');
require_once("$DELIBDIR/php/page.php");
require_once("$DELIBDIR/php/dept.php");
require_once("$DELIBDIR/php/menu.php");
require_once("$DELIBDIR/php/navigator.php");
require_once($DELIBDIR.'/php/inst.php');
require_once($DELIBDIR.'/php/sites.php');

decom_page_init();
$cont = '';
$menu = new DecomMenu(); // TODO set loadData=false

if(!isset($_GET['dept'])) {
	$diRoot = new DecomInstitute(2); // 2 means Calicut University Campus
	$schools = $diRoot->getChildrenIds();

	foreach($schools as $school) {
		$diSchool = new DecomInstitute($school);
		echo $diSchool->getPropertyValue('name').'<br/>';

		$depts = $diSchool->getChildrenIds();
		foreach($depts as $dept) {
			$diDept = new DecomInstitute($dept);;
	
			$nav = new DecomNavigator();
			$nav->setParameter('inst', $dept);

			echo '----'.$diDept->getPropertyValue('name').'<br/>';
		}
	}
	$cont .= $menu->toHtml();
}

decom_page_set_content($cont);
decom_page_display();


?>
