<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/inst.php');
	require_once($DELIBDIR.'/php/cache.php');

	//$ret = decom_cache_put('key1', 'value1');
	//print_r($ret);
	
	foreach(['hello', '__hello', '_hello', '9hello', '__r_a'] as $a) {
		echo "<br/>$a - ";
		print_r(decom_validate_attribute_name($a));
	}
	
	$ret = decom_add_relationship('student_of', 'test110', 'test108', '01', '1n');
	if(is_a($ret, 'DecomError'))
		echo '<br/>'.$ret->getMessageHtml();
?>
