<?php
//	$DEDBDONTCONNECT = true;
	require_once('../delocalconf.php');
	require_once($DELIBDIR.'/php/page.php');
	require_once($DELIBDIR.'/php/cms/gateway.php');

	decom_page_init();
	decom_page_set_title('Welcome!');
	$con = '';
	
	$ret = decom_autoinclude($_GET['p']);
	if(is_a($ret, 'DecomError')) {
		$con = $ret->getMessageHtml();
	}
	else {
		foreach($ret as $path)
			include($path);
	}

	decom_page_set_content($con);
	decom_page_display();
?>
