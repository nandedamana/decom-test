<?php
//	$DEDBDONTCONNECT = true;
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/page.php');
	
	decom_page_init();
	
	$subm1 = new DecomMenu();
	$subm1->addItem(
		new DecomMenuItem('Page 1', '/page1', 'First page'));
	$subm1->addItem(
		new DecomMenuItem('Page 2', '/page2', 'Second page'));
	
	$subm2 = new DecomMenu();
	$subm2->addItem(
		new DecomMenuItem('Page A', '/page3', '3rd page'));
	$subm2->addItem(
		new DecomMenuItem('Page B', '/page4', '4th page'));
	
	$menu = new DecomMenu();
	$menu->addItem(
		new DecomMenuItem('Section 1', '#', 'Section 1', $subm1));
	$menu->addItem(
		new DecomMenuItem('Section 2', '#', 'Section 2', $subm2));
	
	$footer = new DecomPageFooter();
	$footer->setMenu($menu);
	$footer->setAttributionHtml('<p>Copyright (C) 2019 Nandakumar Edamana.</p>');
	decom_page_set_footer($footer);
	
	decom_page_display();
?>
