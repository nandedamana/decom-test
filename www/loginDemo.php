<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/people/auth.php');
	require_once($DELIBDIR.'/php/people/person.php');

	$ret = decom_auth('user1', 'password');

	if(is_a($ret, 'DecomError')) {
		echo 'Error: '.$ret->getMessage();
	}
	else {
		$pobj = new DecomPerson($ret);
		echo '<p>Logged in as: '.$pobj->getUsername();
	}
?>
