<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/inst.php');
	require_once($DELIBDIR.'/php/site.php');
	require_once($DELIBDIR.'/php/page.php');
	
	decom_page_init();

	$iobj = new DecomInstitute(100);
	$title = $iobj->getPropertyValue('name');

	$site = $iobj->getPropertyValue('site'); // TODO check whether site exists
	$sobj = new DecomSite($site);

	decom_page_set_title($title);
	decom_page_set_navbar(
		$sobj->getNavigationMenu(
			$_GET, /* Pass the list of existing GET params so that they'll be added to each menu link */
			'page' /* GET param to pass page paths */
		));

	$con = '';
		
	if(isset($_GET['page'])) {
		$page = &$_GET['page'];

		$con .= 'TODO show page: '.$page;
	}

	decom_page_set_content($con);
	decom_page_display();
?>
