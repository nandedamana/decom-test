<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/page.php');
	require_once($DELIBDIR.'/php/inst.php');
	require_once($DELIBDIR.'/php/sites.php');
	
	$di = new DecomInstitute(2);
	$children = $di->getChildrenIds();
	foreach($children as $child) {
		$inst = new DecomInstitute($child);
		echo $inst->getPropertyValue('name').'<br/>';
		
		if($inst->hasPropertyValue('site')) {
			echo $inst->getPropertyValue('site').'<br/>';
		}
	}
?>
