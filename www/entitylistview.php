<style>
table {
	border-collapse: collapse;
}

table td, table th {
	border: 1px solid gray;
}
</style>
<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/views/entitylisttable.php');

	$v = new DecomEntityListTableView('site');
	echo $v->render();
?>
