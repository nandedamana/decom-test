<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/views/entity.php');

	$v = new DecomEntityView('person', 2);
	echo $v->render();

	/* The following snippet actually loads class data only once. Wrote this just to let you know how efficient it is. */
	/*
	$v = new DecomEntityView('person', 2);
	$v->load('person', null);
	$v->load('person', 2);
	echo $v->render();
	*/
?>
