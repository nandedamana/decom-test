<h1>Private File Serving</h1>

<p>Private files are added using <strong>decom_entity_add_private_file()</strong> and served using <b>decom_serve_private_file()</b>. Adding files is straightforward, but serving is not. To serve private files, you should first create a gateway file in your document root. This file should call decom_serve_private_file() with the parameters it receive via GET or POST. You can then use the location of this file (with parameters) as the src of an img tag or href of a download link.</p>

<p>One major goal of such a file is <b>authentication</b>. You should perform authentication before calling decom_serve_private_file(), or people will get access to the private files of others.</p>

<h2>Use in img tag</h2>

Example:<br/>

<code>
	&lt;img src="serve.php?pid=10&filename=avatar" alt="Avatar" /&gt;
</code>

<p>If you have uploaded such a file, you can see the result right here:</p>
<img src="serve.php?pid=0&filename=avatar" alt="Avatar" />

<h2>Code of serve.php (the gateway)</h2>
<code>
<pre>
<?php
	echo htmlspecialchars(file_get_contents('serve.php'));
?>
</pre>
</code>
