<?php
/**
 * Nandakumar Edamana
 * 2019-07-17
 */

	require_once('../delocalconf.php');
	require_once($DELIBDIR.'/php/serve/privatefile.php');

	if(!isset($_GET['pid'], $_GET['filename'])) {
		http_response_code(400); // Bad request
		exit;
	}

	/* Requested eid and filename */
	$reqeid   = $_GET['pid'];
	$reqfname = $_GET['filename'];

	/* TODO Authentication
	 *
	 * If not admin or another powerful user:
	 *   If the person ID in the session variable does not match $reqeid:
	 *     http_response_code(403); // Forbidden
	 *     exit;
	 *   End if
	 * End if
	 */

	decom_serve_private_file('person', $reqeid, $reqfname);
?>
