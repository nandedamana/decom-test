<?php
	$DEDBDONTCONNECT = true;
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/page.php');
	require_once($DELIBDIR.'/php/dept.php');
	decom_page_init();
	decom_page_set_title('Welcome!');
	
	$subm = new DecomMenu();
	$subm->addItem(
		new DecomMenuItem('Page 1', '/page1', 'First page'));
	$subm->addItem(
		new DecomMenuItem('Page 2', '/page2', 'Second page'));
	
	$nav = new DecomMenu();
	$nav->addItem(
		new DecomMenuItem('Home', './', 'Home page'));
		
	$nav->addItem(
		new DecomMenuItem('Pages',
			'/pages',
			'Page list',
			$subm));
		
	$nav->addItem(
		new DecomMenuItem('Contact', 'contact', 'Contact details'));
	decom_page_set_navbar($nav);
	
	/* Side Menu */
	$sm = new DecomMenu();
	$sm->addItem(
		new DecomMenuItem('Action 1', '#', 'First Action'));
	$sm->addItem(
		new DecomMenuItem('Action 2', '#', 'Second Action'));
	
	$con = '';
	$con .= 'Here goes the first paragraph.';
	
//	decom_page_set_side_menu($sm);
	
	/*
	$dids = decom_get_department_ids();
	foreach($dids as $id) {
		$dept = new DecomDepartment($id);
		$con .= $dept->getName();
	}
	*/
	decom_page_set_content($con);
	decom_page_display();
?>
